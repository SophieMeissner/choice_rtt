﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2023.2.3),
    on Februar 13, 2024, at 16:19
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

import psychopy
psychopy.useVersion('2023.2.3')


# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import plugins
plugins.activatePlugins()
prefs.hardware['audioLib'] = 'ptb'
prefs.hardware['audioLatencyMode'] = '3'
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout
from psychopy.tools import environmenttools
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER, priority)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

import psychopy.iohub as io
from psychopy.hardware import keyboard

# --- Setup global variables (available in all functions) ---
# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
# Store info about the experiment session
psychopyVersion = '2023.2.3'
expName = 'crtt_exercise'  # from the Builder filename that created this script
expInfo = {
    'participant': '',
    'session': '',
    'age': '',
    'left-handed': False,
    'Do you like this session?': ['Yes','No'],
    'output-path': '../../sourcedata',
    'date': data.getDateStr(),  # add a simple timestamp
    'expName': expName,
    'psychopyVersion': psychopyVersion,
}


def showExpInfoDlg(expInfo):
    """
    Show participant info dialog.
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    
    Returns
    ==========
    dict
        Information about this experiment.
    """
    # temporarily remove keys which the dialog doesn't need to show
    poppedKeys = {
        'date': expInfo.pop('date', data.getDateStr()),
        'expName': expInfo.pop('expName', expName),
        'psychopyVersion': expInfo.pop('psychopyVersion', psychopyVersion),
    }
    # show participant info dialog
    dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    # restore hidden keys
    expInfo.update(poppedKeys)
    # return expInfo
    return expInfo


def setupData(expInfo, dataDir=None):
    """
    Make an ExperimentHandler to handle trials and saving.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    dataDir : Path, str or None
        Folder to save the data to, leave as None to create a folder in the current directory.    
    Returns
    ==========
    psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    
    # data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
    if dataDir is None:
        dataDir = _thisDir
    filename = u'%s/sub-%s/ses-%s/%s_%s_%s_%s' % (expInfo['output-path'], expInfo['participant'], expInfo['session'], expInfo['participant'], expInfo['session'], expName, expInfo['date'])
    # make sure filename is relative to dataDir
    if os.path.isabs(filename):
        dataDir = os.path.commonprefix([dataDir, filename])
        filename = os.path.relpath(filename, dataDir)
    
    # an ExperimentHandler isn't essential but helps with data saving
    thisExp = data.ExperimentHandler(
        name=expName, version='',
        extraInfo=expInfo, runtimeInfo=None,
        originPath='C:\\Users\\Sophie\\Documents\\NOWA_school\\choice_rtt\\code\\experiment\\crtt_exercise_lastrun.py',
        savePickle=True, saveWideText=True,
        dataFileName=dataDir + os.sep + filename, sortColumns='time'
    )
    thisExp.setPriority('thisRow.t', priority.CRITICAL)
    thisExp.setPriority('expName', priority.LOW)
    # return experiment handler
    return thisExp


def setupLogging(filename):
    """
    Setup a log file and tell it what level to log at.
    
    Parameters
    ==========
    filename : str or pathlib.Path
        Filename to save log file and data files as, doesn't need an extension.
    
    Returns
    ==========
    psychopy.logging.LogFile
        Text stream to receive inputs from the logging system.
    """
    # this outputs to the screen, not a file
    logging.console.setLevel(logging.EXP)
    # save a log file for detail verbose info
    logFile = logging.LogFile(filename+'.log', level=logging.EXP)
    
    return logFile


def setupWindow(expInfo=None, win=None):
    """
    Setup the Window
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    win : psychopy.visual.Window
        Window to setup - leave as None to create a new window.
    
    Returns
    ==========
    psychopy.visual.Window
        Window in which to run this experiment.
    """
    if win is None:
        # if not given a window to setup, make one
        win = visual.Window(
            size=[1440, 900], fullscr=True, screen=0,
            winType='pyglet', allowStencil=False,
            monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
            backgroundImage='', backgroundFit='none',
            blendMode='avg', useFBO=True,
            units='height'
        )
        if expInfo is not None:
            # store frame rate of monitor if we can measure it
            expInfo['frameRate'] = win.getActualFrameRate()
    else:
        # if we have a window, just set the attributes which are safe to set
        win.color = [0,0,0]
        win.colorSpace = 'rgb'
        win.backgroundImage = ''
        win.backgroundFit = 'none'
        win.units = 'height'
    win.mouseVisible = False
    win.hideMessage()
    return win


def setupInputs(expInfo, thisExp, win):
    """
    Setup whatever inputs are available (mouse, keyboard, eyetracker, etc.)
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    win : psychopy.visual.Window
        Window in which to run this experiment.
    Returns
    ==========
    dict
        Dictionary of input devices by name.
    """
    # --- Setup input devices ---
    inputs = {}
    ioConfig = {}
    
    # Setup iohub keyboard
    ioConfig['Keyboard'] = dict(use_keymap='psychopy')
    
    ioSession = '1'
    if 'session' in expInfo:
        ioSession = str(expInfo['session'])
    ioServer = io.launchHubServer(window=win, **ioConfig)
    eyetracker = None
    
    # create a default keyboard (e.g. to check for escape)
    defaultKeyboard = keyboard.Keyboard(backend='iohub')
    # return inputs dict
    return {
        'ioServer': ioServer,
        'defaultKeyboard': defaultKeyboard,
        'eyetracker': eyetracker,
    }

def pauseExperiment(thisExp, inputs=None, win=None, timers=[], playbackComponents=[]):
    """
    Pause this experiment, preventing the flow from advancing to the next routine until resumed.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    timers : list, tuple
        List of timers to reset once pausing is finished.
    playbackComponents : list, tuple
        List of any components with a `pause` method which need to be paused.
    """
    # if we are not paused, do nothing
    if thisExp.status != PAUSED:
        return
    
    # pause any playback components
    for comp in playbackComponents:
        comp.pause()
    # prevent components from auto-drawing
    win.stashAutoDraw()
    # run a while loop while we wait to unpause
    while thisExp.status == PAUSED:
        # make sure we have a keyboard
        if inputs is None:
            inputs = {
                'defaultKeyboard': keyboard.Keyboard(backend='ioHub')
            }
        # check for quit (typically the Esc key)
        if inputs['defaultKeyboard'].getKeys(keyList=['escape']):
            endExperiment(thisExp, win=win, inputs=inputs)
        # flip the screen
        win.flip()
    # if stop was requested while paused, quit
    if thisExp.status == FINISHED:
        endExperiment(thisExp, inputs=inputs, win=win)
    # resume any playback components
    for comp in playbackComponents:
        comp.play()
    # restore auto-drawn components
    win.retrieveAutoDraw()
    # reset any timers
    for timer in timers:
        timer.reset()


def run(expInfo, thisExp, win, inputs, globalClock=None, thisSession=None):
    """
    Run the experiment flow.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    psychopy.visual.Window
        Window in which to run this experiment.
    inputs : dict
        Dictionary of input devices by name.
    globalClock : psychopy.core.clock.Clock or None
        Clock to get global time from - supply None to make a new one.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    # mark experiment as started
    thisExp.status = STARTED
    # make sure variables created by exec are available globally
    exec = environmenttools.setExecEnvironment(globals())
    # get device handles from dict of input devices
    ioServer = inputs['ioServer']
    defaultKeyboard = inputs['defaultKeyboard']
    eyetracker = inputs['eyetracker']
    # make sure we're running in the directory for this experiment
    os.chdir(_thisDir)
    # get filename from ExperimentHandler for convenience
    filename = thisExp.dataFileName
    frameTolerance = 0.001  # how close to onset before 'same' frame
    endExpNow = False  # flag for 'escape' or other condition => quit the exp
    # get frame duration from frame rate in expInfo
    if 'frameRate' in expInfo and expInfo['frameRate'] is not None:
        frameDur = 1.0 / round(expInfo['frameRate'])
    else:
        frameDur = 1.0 / 60.0  # could not measure, so guess
    
    # Start Code - component code to be run after the window creation
    
    # --- Initialize components for Routine "hello" ---
    welcome_text = visual.TextStim(win=win, name='welcome_text',
        text='Hello!\n\nAnd welcome to the experiment.\n\nPlease press the space bar to continue.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=-1.0);
    spacebar_welcome = keyboard.Keyboard()
    
    # --- Initialize components for Routine "general_instructions" ---
    instruction_text = visual.TextStim(win=win, name='instruction_text',
        text='In this task, you will make decisions as to which stimulus you have seen. \n\nThere are three versions of the task:\n- In the first, you have to decide which shape you have seen.\n- In the second, which image  you have seen.\n- In the third, which sound you have heard. \n\nBefore each task, you will get set of specific instructions and short practice period. \n\nPlease press the space bar to continue.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_instruction = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instr_shapes" ---
    shapeInstr_text = visual.TextStim(win=win, name='shapeInstr_text',
        text='In this task you will make a decision as to which shape you have seen.\n\nPress C or click cross for a cross \nPress V or click square for a square \nPress B or click plus for a plus\n\nFirst, we will have a quick practice.\n\nPush space bar or click / touch one of the buttons to begin.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_shapes = keyboard.Keyboard()
    image_square = visual.ImageStim(
        win=win,
        name='image_square', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    image_plus = visual.ImageStim(
        win=win,
        name='image_plus', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    image_cross = visual.ImageStim(
        win=win,
        name='image_cross', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    
    # --- Initialize components for Routine "trial" ---
    practice_trial_square = visual.ImageStim(
        win=win,
        name='practice_trial_square', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-1.0)
    practice_trial_plus = visual.ImageStim(
        win=win,
        name='practice_trial_plus', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    practice_trial_cross = visual.ImageStim(
        win=win,
        name='practice_trial_cross', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    far_left_tile = visual.ImageStim(
        win=win,
        name='far_left_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    left_tile = visual.ImageStim(
        win=win,
        name='left_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    right_tile = visual.ImageStim(
        win=win,
        name='right_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    far_right_tile = visual.ImageStim(
        win=win,
        name='far_right_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    image = visual.ImageStim(
        win=win,
        name='image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    trial_keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "feedback_screen" ---
    text = visual.TextStim(win=win, name='text',
        text='',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_feedback = keyboard.Keyboard()
    
    # --- Initialize components for Routine "start_actual_trial" ---
    instr_data_collection = visual.TextStim(win=win, name='instr_data_collection',
        text='Practice is over!\n\nNow data collection starts!\n\nPress space bar to start.\n\n',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_start_trials = keyboard.Keyboard()
    
    # --- Initialize components for Routine "trial" ---
    practice_trial_square = visual.ImageStim(
        win=win,
        name='practice_trial_square', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-1.0)
    practice_trial_plus = visual.ImageStim(
        win=win,
        name='practice_trial_plus', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    practice_trial_cross = visual.ImageStim(
        win=win,
        name='practice_trial_cross', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.35), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    far_left_tile = visual.ImageStim(
        win=win,
        name='far_left_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    left_tile = visual.ImageStim(
        win=win,
        name='left_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    right_tile = visual.ImageStim(
        win=win,
        name='right_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    far_right_tile = visual.ImageStim(
        win=win,
        name='far_right_tile', 
        image='C:/Users/Sophie/Documents/NOWA_school/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    image = visual.ImageStim(
        win=win,
        name='image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    trial_keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "feedback_screen" ---
    text = visual.TextStim(win=win, name='text',
        text='',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_feedback = keyboard.Keyboard()
    
    # --- Initialize components for Routine "end_screen" ---
    end_text = visual.TextStim(win=win, name='end_text',
        text='You have reached the end of the experiment, thank you very much for participating. \n\n:)\n\nPlease press the space bar to finish.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_end = keyboard.Keyboard()
    
    # create some handy timers
    if globalClock is None:
        globalClock = core.Clock()  # to track the time since experiment started
    if ioServer is not None:
        ioServer.syncClock(globalClock)
    logging.setDefaultClock(globalClock)
    routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine
    win.flip()  # flip window to reset last flip timer
    # store the exact time the global clock started
    expInfo['expStart'] = data.getDateStr(format='%Y-%m-%d %Hh%M.%S.%f %z', fractionalSecondDigits=6)
    
    # --- Prepare to start Routine "hello" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('hello.started', globalClock.getTime())
    # Run 'Begin Routine' code from code
    # Construct the path with placeholders replaced by actual values
    dir_path = expInfo['output-path'] + "/sub-" + expInfo['participant'] + "/ses-" + expInfo['session']
    
    # Check if the path exists
    if not os.path.exists(dir_path):
        # Create the directory, including all intermediate directories
        os.makedirs(path)
        print(f"Directory '{dir_path}' was created.")
    else:
        print(f"Directory '{dir_path}' already exists.")
    spacebar_welcome.keys = []
    spacebar_welcome.rt = []
    _spacebar_welcome_allKeys = []
    # keep track of which components have finished
    helloComponents = [welcome_text, spacebar_welcome]
    for thisComponent in helloComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "hello" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *welcome_text* updates
        
        # if welcome_text is starting this frame...
        if welcome_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            welcome_text.frameNStart = frameN  # exact frame index
            welcome_text.tStart = t  # local t and not account for scr refresh
            welcome_text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(welcome_text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'welcome_text.started')
            # update status
            welcome_text.status = STARTED
            welcome_text.setAutoDraw(True)
        
        # if welcome_text is active this frame...
        if welcome_text.status == STARTED:
            # update params
            pass
        
        # *spacebar_welcome* updates
        waitOnFlip = False
        
        # if spacebar_welcome is starting this frame...
        if spacebar_welcome.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_welcome.frameNStart = frameN  # exact frame index
            spacebar_welcome.tStart = t  # local t and not account for scr refresh
            spacebar_welcome.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_welcome, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_welcome.started')
            # update status
            spacebar_welcome.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_welcome.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_welcome.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_welcome.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_welcome.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_welcome_allKeys.extend(theseKeys)
            if len(_spacebar_welcome_allKeys):
                spacebar_welcome.keys = _spacebar_welcome_allKeys[-1].name  # just the last key pressed
                spacebar_welcome.rt = _spacebar_welcome_allKeys[-1].rt
                spacebar_welcome.duration = _spacebar_welcome_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in helloComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "hello" ---
    for thisComponent in helloComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('hello.stopped', globalClock.getTime())
    # check responses
    if spacebar_welcome.keys in ['', [], None]:  # No response was made
        spacebar_welcome.keys = None
    thisExp.addData('spacebar_welcome.keys',spacebar_welcome.keys)
    if spacebar_welcome.keys != None:  # we had a response
        thisExp.addData('spacebar_welcome.rt', spacebar_welcome.rt)
        thisExp.addData('spacebar_welcome.duration', spacebar_welcome.duration)
    thisExp.nextEntry()
    # the Routine "hello" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "general_instructions" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('general_instructions.started', globalClock.getTime())
    spacebar_instruction.keys = []
    spacebar_instruction.rt = []
    _spacebar_instruction_allKeys = []
    # keep track of which components have finished
    general_instructionsComponents = [instruction_text, spacebar_instruction]
    for thisComponent in general_instructionsComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "general_instructions" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instruction_text* updates
        
        # if instruction_text is starting this frame...
        if instruction_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruction_text.frameNStart = frameN  # exact frame index
            instruction_text.tStart = t  # local t and not account for scr refresh
            instruction_text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruction_text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruction_text.started')
            # update status
            instruction_text.status = STARTED
            instruction_text.setAutoDraw(True)
        
        # if instruction_text is active this frame...
        if instruction_text.status == STARTED:
            # update params
            pass
        
        # *spacebar_instruction* updates
        waitOnFlip = False
        
        # if spacebar_instruction is starting this frame...
        if spacebar_instruction.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_instruction.frameNStart = frameN  # exact frame index
            spacebar_instruction.tStart = t  # local t and not account for scr refresh
            spacebar_instruction.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_instruction, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_instruction.started')
            # update status
            spacebar_instruction.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_instruction.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_instruction.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_instruction.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_instruction.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_instruction_allKeys.extend(theseKeys)
            if len(_spacebar_instruction_allKeys):
                spacebar_instruction.keys = _spacebar_instruction_allKeys[-1].name  # just the last key pressed
                spacebar_instruction.rt = _spacebar_instruction_allKeys[-1].rt
                spacebar_instruction.duration = _spacebar_instruction_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in general_instructionsComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "general_instructions" ---
    for thisComponent in general_instructionsComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('general_instructions.stopped', globalClock.getTime())
    # check responses
    if spacebar_instruction.keys in ['', [], None]:  # No response was made
        spacebar_instruction.keys = None
    thisExp.addData('spacebar_instruction.keys',spacebar_instruction.keys)
    if spacebar_instruction.keys != None:  # we had a response
        thisExp.addData('spacebar_instruction.rt', spacebar_instruction.rt)
        thisExp.addData('spacebar_instruction.duration', spacebar_instruction.duration)
    thisExp.nextEntry()
    # the Routine "general_instructions" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instr_shapes" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instr_shapes.started', globalClock.getTime())
    spacebar_shapes.keys = []
    spacebar_shapes.rt = []
    _spacebar_shapes_allKeys = []
    # keep track of which components have finished
    instr_shapesComponents = [shapeInstr_text, spacebar_shapes, image_square, image_plus, image_cross]
    for thisComponent in instr_shapesComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instr_shapes" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *shapeInstr_text* updates
        
        # if shapeInstr_text is starting this frame...
        if shapeInstr_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            shapeInstr_text.frameNStart = frameN  # exact frame index
            shapeInstr_text.tStart = t  # local t and not account for scr refresh
            shapeInstr_text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(shapeInstr_text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'shapeInstr_text.started')
            # update status
            shapeInstr_text.status = STARTED
            shapeInstr_text.setAutoDraw(True)
        
        # if shapeInstr_text is active this frame...
        if shapeInstr_text.status == STARTED:
            # update params
            pass
        
        # *spacebar_shapes* updates
        waitOnFlip = False
        
        # if spacebar_shapes is starting this frame...
        if spacebar_shapes.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_shapes.frameNStart = frameN  # exact frame index
            spacebar_shapes.tStart = t  # local t and not account for scr refresh
            spacebar_shapes.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_shapes, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_shapes.started')
            # update status
            spacebar_shapes.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_shapes.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_shapes.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_shapes.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_shapes.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_shapes_allKeys.extend(theseKeys)
            if len(_spacebar_shapes_allKeys):
                spacebar_shapes.keys = _spacebar_shapes_allKeys[-1].name  # just the last key pressed
                spacebar_shapes.rt = _spacebar_shapes_allKeys[-1].rt
                spacebar_shapes.duration = _spacebar_shapes_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # *image_square* updates
        
        # if image_square is starting this frame...
        if image_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_square.frameNStart = frameN  # exact frame index
            image_square.tStart = t  # local t and not account for scr refresh
            image_square.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_square, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'image_square.started')
            # update status
            image_square.status = STARTED
            image_square.setAutoDraw(True)
        
        # if image_square is active this frame...
        if image_square.status == STARTED:
            # update params
            pass
        
        # *image_plus* updates
        
        # if image_plus is starting this frame...
        if image_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_plus.frameNStart = frameN  # exact frame index
            image_plus.tStart = t  # local t and not account for scr refresh
            image_plus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_plus, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'image_plus.started')
            # update status
            image_plus.status = STARTED
            image_plus.setAutoDraw(True)
        
        # if image_plus is active this frame...
        if image_plus.status == STARTED:
            # update params
            pass
        
        # *image_cross* updates
        
        # if image_cross is starting this frame...
        if image_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_cross.frameNStart = frameN  # exact frame index
            image_cross.tStart = t  # local t and not account for scr refresh
            image_cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_cross, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'image_cross.started')
            # update status
            image_cross.status = STARTED
            image_cross.setAutoDraw(True)
        
        # if image_cross is active this frame...
        if image_cross.status == STARTED:
            # update params
            pass
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instr_shapesComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instr_shapes" ---
    for thisComponent in instr_shapesComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instr_shapes.stopped', globalClock.getTime())
    # check responses
    if spacebar_shapes.keys in ['', [], None]:  # No response was made
        spacebar_shapes.keys = None
    thisExp.addData('spacebar_shapes.keys',spacebar_shapes.keys)
    if spacebar_shapes.keys != None:  # we had a response
        thisExp.addData('spacebar_shapes.rt', spacebar_shapes.rt)
        thisExp.addData('spacebar_shapes.duration', spacebar_shapes.duration)
    thisExp.nextEntry()
    # the Routine "instr_shapes" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    trials_loop = data.TrialHandler(nReps=1.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('shapes_targets.csv'),
        seed=None, name='trials_loop')
    thisExp.addLoop(trials_loop)  # add the loop to the experiment
    thisTrials_loop = trials_loop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrials_loop.rgb)
    if thisTrials_loop != None:
        for paramName in thisTrials_loop:
            globals()[paramName] = thisTrials_loop[paramName]
    
    for thisTrials_loop in trials_loop:
        currentLoop = trials_loop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisTrials_loop.rgb)
        if thisTrials_loop != None:
            for paramName in thisTrials_loop:
                globals()[paramName] = thisTrials_loop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('trial.started', globalClock.getTime())
        # Run 'Begin Routine' code from jitter_targetposition
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        image.setPos((position_trial, 0))
        image.setImage(TargetImage)
        trial_keyboard_response.keys = []
        trial_keyboard_response.rt = []
        _trial_keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [practice_trial_square, practice_trial_plus, practice_trial_cross, far_left_tile, left_tile, right_tile, far_right_tile, image, trial_keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *practice_trial_square* updates
            
            # if practice_trial_square is starting this frame...
            if practice_trial_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_square.frameNStart = frameN  # exact frame index
                practice_trial_square.tStart = t  # local t and not account for scr refresh
                practice_trial_square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_square.started')
                # update status
                practice_trial_square.status = STARTED
                practice_trial_square.setAutoDraw(True)
            
            # if practice_trial_square is active this frame...
            if practice_trial_square.status == STARTED:
                # update params
                pass
            
            # *practice_trial_plus* updates
            
            # if practice_trial_plus is starting this frame...
            if practice_trial_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_plus.frameNStart = frameN  # exact frame index
                practice_trial_plus.tStart = t  # local t and not account for scr refresh
                practice_trial_plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_plus.started')
                # update status
                practice_trial_plus.status = STARTED
                practice_trial_plus.setAutoDraw(True)
            
            # if practice_trial_plus is active this frame...
            if practice_trial_plus.status == STARTED:
                # update params
                pass
            
            # *practice_trial_cross* updates
            
            # if practice_trial_cross is starting this frame...
            if practice_trial_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_cross.frameNStart = frameN  # exact frame index
                practice_trial_cross.tStart = t  # local t and not account for scr refresh
                practice_trial_cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_cross.started')
                # update status
                practice_trial_cross.status = STARTED
                practice_trial_cross.setAutoDraw(True)
            
            # if practice_trial_cross is active this frame...
            if practice_trial_cross.status == STARTED:
                # update params
                pass
            
            # *far_left_tile* updates
            
            # if far_left_tile is starting this frame...
            if far_left_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                far_left_tile.frameNStart = frameN  # exact frame index
                far_left_tile.tStart = t  # local t and not account for scr refresh
                far_left_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(far_left_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'far_left_tile.started')
                # update status
                far_left_tile.status = STARTED
                far_left_tile.setAutoDraw(True)
            
            # if far_left_tile is active this frame...
            if far_left_tile.status == STARTED:
                # update params
                pass
            
            # *left_tile* updates
            
            # if left_tile is starting this frame...
            if left_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_tile.frameNStart = frameN  # exact frame index
                left_tile.tStart = t  # local t and not account for scr refresh
                left_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_tile.started')
                # update status
                left_tile.status = STARTED
                left_tile.setAutoDraw(True)
            
            # if left_tile is active this frame...
            if left_tile.status == STARTED:
                # update params
                pass
            
            # *right_tile* updates
            
            # if right_tile is starting this frame...
            if right_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_tile.frameNStart = frameN  # exact frame index
                right_tile.tStart = t  # local t and not account for scr refresh
                right_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_tile.started')
                # update status
                right_tile.status = STARTED
                right_tile.setAutoDraw(True)
            
            # if right_tile is active this frame...
            if right_tile.status == STARTED:
                # update params
                pass
            
            # *far_right_tile* updates
            
            # if far_right_tile is starting this frame...
            if far_right_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                far_right_tile.frameNStart = frameN  # exact frame index
                far_right_tile.tStart = t  # local t and not account for scr refresh
                far_right_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(far_right_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'far_right_tile.started')
                # update status
                far_right_tile.status = STARTED
                far_right_tile.setAutoDraw(True)
            
            # if far_right_tile is active this frame...
            if far_right_tile.status == STARTED:
                # update params
                pass
            
            # *image* updates
            
            # if image is starting this frame...
            if image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                image.frameNStart = frameN  # exact frame index
                image.tStart = t  # local t and not account for scr refresh
                image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image.started')
                # update status
                image.status = STARTED
                image.setAutoDraw(True)
            
            # if image is active this frame...
            if image.status == STARTED:
                # update params
                pass
            
            # if image is stopping this frame...
            if image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    image.tStop = t  # not accounting for scr refresh
                    image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'image.stopped')
                    # update status
                    image.status = FINISHED
                    image.setAutoDraw(False)
            
            # *trial_keyboard_response* updates
            waitOnFlip = False
            
            # if trial_keyboard_response is starting this frame...
            if trial_keyboard_response.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                # keep track of start time/frame for later
                trial_keyboard_response.frameNStart = frameN  # exact frame index
                trial_keyboard_response.tStart = t  # local t and not account for scr refresh
                trial_keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(trial_keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'trial_keyboard_response.started')
                # update status
                trial_keyboard_response.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(trial_keyboard_response.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(trial_keyboard_response.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if trial_keyboard_response.status == STARTED and not waitOnFlip:
                theseKeys = trial_keyboard_response.getKeys(keyList=['c','v','b'], ignoreKeys=["escape"], waitRelease=False)
                _trial_keyboard_response_allKeys.extend(theseKeys)
                if len(_trial_keyboard_response_allKeys):
                    trial_keyboard_response.keys = _trial_keyboard_response_allKeys[-1].name  # just the last key pressed
                    trial_keyboard_response.rt = _trial_keyboard_response_allKeys[-1].rt
                    trial_keyboard_response.duration = _trial_keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (trial_keyboard_response.keys == str(corrAns)) or (trial_keyboard_response.keys == corrAns):
                        trial_keyboard_response.corr = 1
                    else:
                        trial_keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # check responses
        if trial_keyboard_response.keys in ['', [], None]:  # No response was made
            trial_keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               trial_keyboard_response.corr = 1;  # correct non-response
            else:
               trial_keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for trials_loop (TrialHandler)
        trials_loop.addData('trial_keyboard_response.keys',trial_keyboard_response.keys)
        trials_loop.addData('trial_keyboard_response.corr', trial_keyboard_response.corr)
        if trial_keyboard_response.keys != None:  # we had a response
            trials_loop.addData('trial_keyboard_response.rt', trial_keyboard_response.rt)
            trials_loop.addData('trial_keyboard_response.duration', trial_keyboard_response.duration)
        # Run 'End Routine' code from feedback_code
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = trial_keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if trial_keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "feedback_screen" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('feedback_screen.started', globalClock.getTime())
        text.setText("The last recorded RT was: "  + str(round(thisRecRT, 3)) + " \nThe response was: "  + thisAcc + " \n\nPress the space bar to continue."  
        )
        spacebar_feedback.keys = []
        spacebar_feedback.rt = []
        _spacebar_feedback_allKeys = []
        # keep track of which components have finished
        feedback_screenComponents = [text, spacebar_feedback]
        for thisComponent in feedback_screenComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "feedback_screen" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *text* updates
            
            # if text is starting this frame...
            if text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                text.frameNStart = frameN  # exact frame index
                text.tStart = t  # local t and not account for scr refresh
                text.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(text, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'text.started')
                # update status
                text.status = STARTED
                text.setAutoDraw(True)
            
            # if text is active this frame...
            if text.status == STARTED:
                # update params
                pass
            
            # *spacebar_feedback* updates
            waitOnFlip = False
            
            # if spacebar_feedback is starting this frame...
            if spacebar_feedback.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                spacebar_feedback.frameNStart = frameN  # exact frame index
                spacebar_feedback.tStart = t  # local t and not account for scr refresh
                spacebar_feedback.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(spacebar_feedback, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'spacebar_feedback.started')
                # update status
                spacebar_feedback.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(spacebar_feedback.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(spacebar_feedback.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if spacebar_feedback.status == STARTED and not waitOnFlip:
                theseKeys = spacebar_feedback.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
                _spacebar_feedback_allKeys.extend(theseKeys)
                if len(_spacebar_feedback_allKeys):
                    spacebar_feedback.keys = _spacebar_feedback_allKeys[-1].name  # just the last key pressed
                    spacebar_feedback.rt = _spacebar_feedback_allKeys[-1].rt
                    spacebar_feedback.duration = _spacebar_feedback_allKeys[-1].duration
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in feedback_screenComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "feedback_screen" ---
        for thisComponent in feedback_screenComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('feedback_screen.stopped', globalClock.getTime())
        # check responses
        if spacebar_feedback.keys in ['', [], None]:  # No response was made
            spacebar_feedback.keys = None
        trials_loop.addData('spacebar_feedback.keys',spacebar_feedback.keys)
        if spacebar_feedback.keys != None:  # we had a response
            trials_loop.addData('spacebar_feedback.rt', spacebar_feedback.rt)
            trials_loop.addData('spacebar_feedback.duration', spacebar_feedback.duration)
        # the Routine "feedback_screen" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 1.0 repeats of 'trials_loop'
    
    
    # --- Prepare to start Routine "start_actual_trial" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('start_actual_trial.started', globalClock.getTime())
    spacebar_start_trials.keys = []
    spacebar_start_trials.rt = []
    _spacebar_start_trials_allKeys = []
    # keep track of which components have finished
    start_actual_trialComponents = [instr_data_collection, spacebar_start_trials]
    for thisComponent in start_actual_trialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "start_actual_trial" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instr_data_collection* updates
        
        # if instr_data_collection is starting this frame...
        if instr_data_collection.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instr_data_collection.frameNStart = frameN  # exact frame index
            instr_data_collection.tStart = t  # local t and not account for scr refresh
            instr_data_collection.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instr_data_collection, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instr_data_collection.started')
            # update status
            instr_data_collection.status = STARTED
            instr_data_collection.setAutoDraw(True)
        
        # if instr_data_collection is active this frame...
        if instr_data_collection.status == STARTED:
            # update params
            pass
        
        # *spacebar_start_trials* updates
        waitOnFlip = False
        
        # if spacebar_start_trials is starting this frame...
        if spacebar_start_trials.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_start_trials.frameNStart = frameN  # exact frame index
            spacebar_start_trials.tStart = t  # local t and not account for scr refresh
            spacebar_start_trials.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_start_trials, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_start_trials.started')
            # update status
            spacebar_start_trials.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_start_trials.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_start_trials.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_start_trials.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_start_trials.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_start_trials_allKeys.extend(theseKeys)
            if len(_spacebar_start_trials_allKeys):
                spacebar_start_trials.keys = _spacebar_start_trials_allKeys[-1].name  # just the last key pressed
                spacebar_start_trials.rt = _spacebar_start_trials_allKeys[-1].rt
                spacebar_start_trials.duration = _spacebar_start_trials_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in start_actual_trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "start_actual_trial" ---
    for thisComponent in start_actual_trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('start_actual_trial.stopped', globalClock.getTime())
    # check responses
    if spacebar_start_trials.keys in ['', [], None]:  # No response was made
        spacebar_start_trials.keys = None
    thisExp.addData('spacebar_start_trials.keys',spacebar_start_trials.keys)
    if spacebar_start_trials.keys != None:  # we had a response
        thisExp.addData('spacebar_start_trials.rt', spacebar_start_trials.rt)
        thisExp.addData('spacebar_start_trials.duration', spacebar_start_trials.duration)
    thisExp.nextEntry()
    # the Routine "start_actual_trial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    data_collection_loop = data.TrialHandler(nReps=1.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('shapes_targets.csv'),
        seed=None, name='data_collection_loop')
    thisExp.addLoop(data_collection_loop)  # add the loop to the experiment
    thisData_collection_loop = data_collection_loop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisData_collection_loop.rgb)
    if thisData_collection_loop != None:
        for paramName in thisData_collection_loop:
            globals()[paramName] = thisData_collection_loop[paramName]
    
    for thisData_collection_loop in data_collection_loop:
        currentLoop = data_collection_loop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisData_collection_loop.rgb)
        if thisData_collection_loop != None:
            for paramName in thisData_collection_loop:
                globals()[paramName] = thisData_collection_loop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('trial.started', globalClock.getTime())
        # Run 'Begin Routine' code from jitter_targetposition
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        image.setPos((position_trial, 0))
        image.setImage(TargetImage)
        trial_keyboard_response.keys = []
        trial_keyboard_response.rt = []
        _trial_keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [practice_trial_square, practice_trial_plus, practice_trial_cross, far_left_tile, left_tile, right_tile, far_right_tile, image, trial_keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *practice_trial_square* updates
            
            # if practice_trial_square is starting this frame...
            if practice_trial_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_square.frameNStart = frameN  # exact frame index
                practice_trial_square.tStart = t  # local t and not account for scr refresh
                practice_trial_square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_square.started')
                # update status
                practice_trial_square.status = STARTED
                practice_trial_square.setAutoDraw(True)
            
            # if practice_trial_square is active this frame...
            if practice_trial_square.status == STARTED:
                # update params
                pass
            
            # *practice_trial_plus* updates
            
            # if practice_trial_plus is starting this frame...
            if practice_trial_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_plus.frameNStart = frameN  # exact frame index
                practice_trial_plus.tStart = t  # local t and not account for scr refresh
                practice_trial_plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_plus.started')
                # update status
                practice_trial_plus.status = STARTED
                practice_trial_plus.setAutoDraw(True)
            
            # if practice_trial_plus is active this frame...
            if practice_trial_plus.status == STARTED:
                # update params
                pass
            
            # *practice_trial_cross* updates
            
            # if practice_trial_cross is starting this frame...
            if practice_trial_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_cross.frameNStart = frameN  # exact frame index
                practice_trial_cross.tStart = t  # local t and not account for scr refresh
                practice_trial_cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_cross.started')
                # update status
                practice_trial_cross.status = STARTED
                practice_trial_cross.setAutoDraw(True)
            
            # if practice_trial_cross is active this frame...
            if practice_trial_cross.status == STARTED:
                # update params
                pass
            
            # *far_left_tile* updates
            
            # if far_left_tile is starting this frame...
            if far_left_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                far_left_tile.frameNStart = frameN  # exact frame index
                far_left_tile.tStart = t  # local t and not account for scr refresh
                far_left_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(far_left_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'far_left_tile.started')
                # update status
                far_left_tile.status = STARTED
                far_left_tile.setAutoDraw(True)
            
            # if far_left_tile is active this frame...
            if far_left_tile.status == STARTED:
                # update params
                pass
            
            # *left_tile* updates
            
            # if left_tile is starting this frame...
            if left_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_tile.frameNStart = frameN  # exact frame index
                left_tile.tStart = t  # local t and not account for scr refresh
                left_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_tile.started')
                # update status
                left_tile.status = STARTED
                left_tile.setAutoDraw(True)
            
            # if left_tile is active this frame...
            if left_tile.status == STARTED:
                # update params
                pass
            
            # *right_tile* updates
            
            # if right_tile is starting this frame...
            if right_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_tile.frameNStart = frameN  # exact frame index
                right_tile.tStart = t  # local t and not account for scr refresh
                right_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_tile.started')
                # update status
                right_tile.status = STARTED
                right_tile.setAutoDraw(True)
            
            # if right_tile is active this frame...
            if right_tile.status == STARTED:
                # update params
                pass
            
            # *far_right_tile* updates
            
            # if far_right_tile is starting this frame...
            if far_right_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                far_right_tile.frameNStart = frameN  # exact frame index
                far_right_tile.tStart = t  # local t and not account for scr refresh
                far_right_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(far_right_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'far_right_tile.started')
                # update status
                far_right_tile.status = STARTED
                far_right_tile.setAutoDraw(True)
            
            # if far_right_tile is active this frame...
            if far_right_tile.status == STARTED:
                # update params
                pass
            
            # *image* updates
            
            # if image is starting this frame...
            if image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                image.frameNStart = frameN  # exact frame index
                image.tStart = t  # local t and not account for scr refresh
                image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image.started')
                # update status
                image.status = STARTED
                image.setAutoDraw(True)
            
            # if image is active this frame...
            if image.status == STARTED:
                # update params
                pass
            
            # if image is stopping this frame...
            if image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    image.tStop = t  # not accounting for scr refresh
                    image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'image.stopped')
                    # update status
                    image.status = FINISHED
                    image.setAutoDraw(False)
            
            # *trial_keyboard_response* updates
            waitOnFlip = False
            
            # if trial_keyboard_response is starting this frame...
            if trial_keyboard_response.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                # keep track of start time/frame for later
                trial_keyboard_response.frameNStart = frameN  # exact frame index
                trial_keyboard_response.tStart = t  # local t and not account for scr refresh
                trial_keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(trial_keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'trial_keyboard_response.started')
                # update status
                trial_keyboard_response.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(trial_keyboard_response.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(trial_keyboard_response.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if trial_keyboard_response.status == STARTED and not waitOnFlip:
                theseKeys = trial_keyboard_response.getKeys(keyList=['c','v','b'], ignoreKeys=["escape"], waitRelease=False)
                _trial_keyboard_response_allKeys.extend(theseKeys)
                if len(_trial_keyboard_response_allKeys):
                    trial_keyboard_response.keys = _trial_keyboard_response_allKeys[-1].name  # just the last key pressed
                    trial_keyboard_response.rt = _trial_keyboard_response_allKeys[-1].rt
                    trial_keyboard_response.duration = _trial_keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (trial_keyboard_response.keys == str(corrAns)) or (trial_keyboard_response.keys == corrAns):
                        trial_keyboard_response.corr = 1
                    else:
                        trial_keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # check responses
        if trial_keyboard_response.keys in ['', [], None]:  # No response was made
            trial_keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               trial_keyboard_response.corr = 1;  # correct non-response
            else:
               trial_keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for data_collection_loop (TrialHandler)
        data_collection_loop.addData('trial_keyboard_response.keys',trial_keyboard_response.keys)
        data_collection_loop.addData('trial_keyboard_response.corr', trial_keyboard_response.corr)
        if trial_keyboard_response.keys != None:  # we had a response
            data_collection_loop.addData('trial_keyboard_response.rt', trial_keyboard_response.rt)
            data_collection_loop.addData('trial_keyboard_response.duration', trial_keyboard_response.duration)
        # Run 'End Routine' code from feedback_code
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = trial_keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if trial_keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "feedback_screen" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('feedback_screen.started', globalClock.getTime())
        text.setText("The last recorded RT was: "  + str(round(thisRecRT, 3)) + " \nThe response was: "  + thisAcc + " \n\nPress the space bar to continue."  
        )
        spacebar_feedback.keys = []
        spacebar_feedback.rt = []
        _spacebar_feedback_allKeys = []
        # keep track of which components have finished
        feedback_screenComponents = [text, spacebar_feedback]
        for thisComponent in feedback_screenComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "feedback_screen" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *text* updates
            
            # if text is starting this frame...
            if text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                text.frameNStart = frameN  # exact frame index
                text.tStart = t  # local t and not account for scr refresh
                text.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(text, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'text.started')
                # update status
                text.status = STARTED
                text.setAutoDraw(True)
            
            # if text is active this frame...
            if text.status == STARTED:
                # update params
                pass
            
            # *spacebar_feedback* updates
            waitOnFlip = False
            
            # if spacebar_feedback is starting this frame...
            if spacebar_feedback.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                spacebar_feedback.frameNStart = frameN  # exact frame index
                spacebar_feedback.tStart = t  # local t and not account for scr refresh
                spacebar_feedback.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(spacebar_feedback, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'spacebar_feedback.started')
                # update status
                spacebar_feedback.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(spacebar_feedback.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(spacebar_feedback.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if spacebar_feedback.status == STARTED and not waitOnFlip:
                theseKeys = spacebar_feedback.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
                _spacebar_feedback_allKeys.extend(theseKeys)
                if len(_spacebar_feedback_allKeys):
                    spacebar_feedback.keys = _spacebar_feedback_allKeys[-1].name  # just the last key pressed
                    spacebar_feedback.rt = _spacebar_feedback_allKeys[-1].rt
                    spacebar_feedback.duration = _spacebar_feedback_allKeys[-1].duration
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in feedback_screenComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "feedback_screen" ---
        for thisComponent in feedback_screenComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('feedback_screen.stopped', globalClock.getTime())
        # check responses
        if spacebar_feedback.keys in ['', [], None]:  # No response was made
            spacebar_feedback.keys = None
        data_collection_loop.addData('spacebar_feedback.keys',spacebar_feedback.keys)
        if spacebar_feedback.keys != None:  # we had a response
            data_collection_loop.addData('spacebar_feedback.rt', spacebar_feedback.rt)
            data_collection_loop.addData('spacebar_feedback.duration', spacebar_feedback.duration)
        # the Routine "feedback_screen" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 1.0 repeats of 'data_collection_loop'
    
    
    # --- Prepare to start Routine "end_screen" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('end_screen.started', globalClock.getTime())
    spacebar_end.keys = []
    spacebar_end.rt = []
    _spacebar_end_allKeys = []
    # keep track of which components have finished
    end_screenComponents = [end_text, spacebar_end]
    for thisComponent in end_screenComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "end_screen" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *end_text* updates
        
        # if end_text is starting this frame...
        if end_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            end_text.frameNStart = frameN  # exact frame index
            end_text.tStart = t  # local t and not account for scr refresh
            end_text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(end_text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'end_text.started')
            # update status
            end_text.status = STARTED
            end_text.setAutoDraw(True)
        
        # if end_text is active this frame...
        if end_text.status == STARTED:
            # update params
            pass
        
        # *spacebar_end* updates
        waitOnFlip = False
        
        # if spacebar_end is starting this frame...
        if spacebar_end.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_end.frameNStart = frameN  # exact frame index
            spacebar_end.tStart = t  # local t and not account for scr refresh
            spacebar_end.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_end, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_end.started')
            # update status
            spacebar_end.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_end.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_end.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_end.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_end.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_end_allKeys.extend(theseKeys)
            if len(_spacebar_end_allKeys):
                spacebar_end.keys = _spacebar_end_allKeys[-1].name  # just the last key pressed
                spacebar_end.rt = _spacebar_end_allKeys[-1].rt
                spacebar_end.duration = _spacebar_end_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in end_screenComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "end_screen" ---
    for thisComponent in end_screenComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('end_screen.stopped', globalClock.getTime())
    # check responses
    if spacebar_end.keys in ['', [], None]:  # No response was made
        spacebar_end.keys = None
    thisExp.addData('spacebar_end.keys',spacebar_end.keys)
    if spacebar_end.keys != None:  # we had a response
        thisExp.addData('spacebar_end.rt', spacebar_end.rt)
        thisExp.addData('spacebar_end.duration', spacebar_end.duration)
    thisExp.nextEntry()
    # the Routine "end_screen" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # mark experiment as finished
    endExperiment(thisExp, win=win, inputs=inputs)


def saveData(thisExp):
    """
    Save data from this experiment
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    filename = thisExp.dataFileName
    # these shouldn't be strictly necessary (should auto-save)
    thisExp.saveAsWideText(filename + '.csv', delim='auto')
    thisExp.saveAsPickle(filename)


def endExperiment(thisExp, inputs=None, win=None):
    """
    End this experiment, performing final shut down operations.
    
    This function does NOT close the window or end the Python process - use `quit` for this.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    """
    if win is not None:
        # remove autodraw from all current components
        win.clearAutoDraw()
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed
        win.flip()
    # mark experiment handler as finished
    thisExp.status = FINISHED
    # shut down eyetracker, if there is one
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()


def quit(thisExp, win=None, inputs=None, thisSession=None):
    """
    Fully quit, closing the window and ending the Python process.
    
    Parameters
    ==========
    win : psychopy.visual.Window
        Window to close.
    inputs : dict
        Dictionary of input devices by name.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    thisExp.abort()  # or data files will save again on exit
    # make sure everything is closed down
    if win is not None:
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed before quitting
        win.flip()
        win.close()
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()
    if thisSession is not None:
        thisSession.stop()
    # terminate Python process
    core.quit()


# if running this experiment as a script...
if __name__ == '__main__':
    # call all functions in order
    expInfo = showExpInfoDlg(expInfo=expInfo)
    thisExp = setupData(expInfo=expInfo)
    logFile = setupLogging(filename=thisExp.dataFileName)
    win = setupWindow(expInfo=expInfo)
    inputs = setupInputs(expInfo=expInfo, thisExp=thisExp, win=win)
    run(
        expInfo=expInfo, 
        thisExp=thisExp, 
        win=win, 
        inputs=inputs
    )
    saveData(thisExp=thisExp)
    quit(thisExp=thisExp, win=win, inputs=inputs)
